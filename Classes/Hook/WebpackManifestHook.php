<?php

namespace Hn\WebpackIntegration\Hook;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\TimeTracker\TimeTracker;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class WebpackManifestHook
{
    private $manifestCache = [];

    private function getManifest(string $dirname)
    {
        if (isset($this->manifestCache[$dirname])) {
            return $this->manifestCache[$dirname];
        }

        $manifestFilename = Environment::getPublicPath() . '/' . $dirname . '/manifest.json';
        if (!file_exists($manifestFilename)) {
            return $this->manifestCache[$dirname] = [];
        }

        $manifest = json_decode(file_get_contents($manifestFilename), true);
        if (json_last_error()) {
            trigger_error("failed to read json file $manifestFilename: " . json_last_error_msg(), E_USER_ERROR);
            return $this->manifestCache[$dirname] = [];
        }

        foreach ($manifest as &$item) {
            if (!is_string($item)) {
                continue;
            }

            // map wildcard ip to the current hostname
            $item = str_replace('0.0.0.0', GeneralUtility::getIndpEnv('TYPO3_HOST_ONLY'), $item);
        }

        return $this->manifestCache[$dirname] = $manifest;
    }

    public function execute(&$params, &$pagerenderer)
    {
        $timeTracker = GeneralUtility::makeInstance(TimeTracker::class);
        $timeTracker->push('check for manifest files');

        $everyFileIsFromWebpack = true;

        foreach (['jsLibs', 'jsFooterLibs', 'jsFiles', 'jsFooterFiles', 'cssLibs', 'cssFiles'] as $fileCategory) {

            // The filename is the key in this array
            // Because I need to preserve the order, I need to rebuild the array entirely.
            // That is why an array map wouldn't work

            $result = [];
            foreach ($params[$fileCategory] as $settings) {
                $file = $settings['file'];
                $manifest = $this->getManifest(dirname($file));
                if (isset($manifest[$file])) {
                    $file = $manifest[$file];
                    $settings['file'] = $file;
                    $settings['compress'] = false;
                    $settings['excludeFromConcatenation'] = true;
                } else if (file_exists($file)) {
                    $everyFileIsFromWebpack = false;
                } else if (is_dir(dirname($file))) {
                    // when you run webpack in --hot mode, css files need to be inlined in the js bundle
                    // this means the css file does not exist ~ that is this case
                    continue;
                }
                $result[$file] = $settings;
            }
            $params[$fileCategory] = $result;
        }

        foreach($params['headerData'] as &$headerString) {
            $matches = null;
            preg_match_all('/href="([^"]+)"/', $headerString, $matches);

            foreach($matches[1] as $filePath) {
                $manifest = $this->getManifest(dirname($filePath));
                $resolvedFilename = $manifest[$filePath] ?? '';
                if ($resolvedFilename) {
                    $headerString = str_replace('href="' . $filePath . '"', 'href="' . $resolvedFilename . '"', $headerString);
                    $everyFileIsFromWebpack = false;
                }
            }
        }

        if ($everyFileIsFromWebpack) {
            // i'd love to disable this check just for the few webpack files but that isn't possible
            // typo3 always checks if it can read the file and if it can, it'll add the mtime
            //
            // the mtime is actually harmful in this case since the file might have been rebuild but the content is the same
            // webpacks versioning will use a checksum of the file which is much more accurate
            //
            // also: this would normally be the wrong place to change this configuration
            // but since this hook is called every time resources are included this is a just in time change
            $GLOBALS['TYPO3_CONF_VARS']['FE']['versionNumberInFilename'] = '';
        }

        $timeTracker->pull();
    }
}
