<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'hauptsache.net webpack integration extension',
    'description' => '',
    'category' => 'misc',
    'state' => 'excludeFromUpdates',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'constraints' => [
        'depends' => [
            'typo3' => '11.0.0-12.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'author' => '',
    'author_email' => '',
    'author_company' => '',
];
