<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(function ($extkey) {
    // add hook to respect webpack manifest
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][] =
        \Hn\WebpackIntegration\Hook\WebpackManifestHook::class . '->execute';

}, 'webpack_integration');
